﻿#Requires -RunAsAdministrator

Param(
    [Parameter(Mandatory=$True)]
        [String]$VCenterServer
)

function Select-TargetVM()
{
    $VMSelection = Get-VM | Out-GridView -OutputMode Multiple
    Return $VMSelection   
}

function VFRC-Callback()
{
    Param(
        [String]$CacheSizeGB_Value,
        [String]$CacheBlockSizeKB_Value
    )

    ForEach($VM in $VMSelection)
    {    
        $HardDisk = Get-HardDisk -VM $VM | Out-GridView -OutputMode Single
        $CurrentVFlashConfig = Get-HardDiskVFlashConfiguration -HardDisk $HardDisk
        Set-HardDiskVFlashConfiguration -VFlashConfiguration $CurrentVFlashConfig -CacheSizeGB $CacheSizeGB_Value -CacheBlockSizeKB $CacheBlockSizeKB_Value -Confirm:$false
    }
}

function VRFCParameterBox()
{

    Add-Type -AssemblyName System.Windows.Forms
    [System.Windows.Forms.Application]::EnableVisualStyles()

    #region begin GUI{ 

    $VFRCDetailsInput                = New-Object system.Windows.Forms.Form
    $VFRCDetailsInput.ClientSize     = '349,181'
    $VFRCDetailsInput.text           = "Flash Cache Parameters"
    $VFRCDetailsInput.TopMost        = $false
    $VFRCDetailsInput.FormBorderStyle = 'Fixed3D'
    $VFRCDetailsInput.MaximizeBox    = $false

    $CacheSizeGB                     = New-Object system.Windows.Forms.Label
    $CacheSizeGB.text                = "Cache Size (GB)"
    $CacheSizeGB.AutoSize            = $true
    $CacheSizeGB.width               = 25
    $CacheSizeGB.height              = 10
    $CacheSizeGB.location            = New-Object System.Drawing.Point(13,17)
    $CacheSizeGB.Font                = 'Microsoft Sans Serif,10'

    $CacheSizeGB_Value               = New-Object system.Windows.Forms.TextBox
    $CacheSizeGB_Value.multiline     = $false
    $CacheSizeGB_Value.width         = 322
    $CacheSizeGB_Value.height        = 20
    $CacheSizeGB_Value.location      = New-Object System.Drawing.Point(13,39)
    $CacheSizeGB_Value.Font          = 'Microsoft Sans Serif,10'

    $CacheBlockSize                  = New-Object system.Windows.Forms.Label
    $CacheBlockSize.text             = "Cache Block Size (KB)"
    $CacheBlockSize.AutoSize         = $true
    $CacheBlockSize.width            = 25
    $CacheBlockSize.height           = 10
    $CacheBlockSize.location         = New-Object System.Drawing.Point(13,70)
    $CacheBlockSize.Font             = 'Microsoft Sans Serif,10'

    $CacheBlockSize_Value            = New-Object system.Windows.Forms.ComboBox
    $CacheBlockSize_Value.width      = 323
    $CacheBlockSize_Value.height     = 20
    @('4','8','16','32','64','128','256','512','1024') | ForEach-Object {[void] $CacheBlockSize_Value.Items.Add($_)}
    $CacheBlockSize_Value.location   = New-Object System.Drawing.Point(13,93)
    $CacheBlockSize_Value.Font       = 'Microsoft Sans Serif,10'

    $SubmitButton                    = New-Object system.Windows.Forms.Button
    $SubmitButton.text               = "Submit"
    $SubmitButton.width              = 74
    $SubmitButton.height             = 40
    $SubmitButton.location           = New-Object System.Drawing.Point(261,123)
    $SubmitButton.Font               = 'Microsoft Sans Serif,10'

    $VFRCDetailsInput.controls.AddRange(@($CacheSizeGB,$CacheSizeGB_Value,$CacheBlockSize,$CacheBlockSize_Value,$SubmitButton))

    $SubmitButton.Add_Click({

        [void]$VFRCDetailsInput.Close()
        VFRC-Callback $CacheSizeGB_Value.Text $CacheBlockSize_Value.SelectedItem                
    })

    [void]$VFRCDetailsInput.ShowDialog()

}

function ImportPowerCLI
{
    param(
    [bool]$promptForCEIP = $false
    )

    # List of modules to be loaded
    $moduleList = @(
        "VMware.VimAutomation.Core",
        "VMware.VimAutomation.Vds",
        "VMware.VimAutomation.Cloud",
        "VMware.VimAutomation.PCloud",
        "VMware.VimAutomation.Cis.Core",
        "VMware.VimAutomation.Storage",
        "VMware.VimAutomation.HorizonView",
        "VMware.VimAutomation.HA",
        "VMware.VimAutomation.vROps",
        "VMware.VumAutomation",
        "VMware.DeployAutomation",
        "VMware.ImageBuilder",
        "VMware.VimAutomation.License",
        "VMware.VimAutomation.Extensions"
        )

    $productName = "PowerCLI"
    $productShortName = "PowerCLI"

    $loadingActivity = "Loading $productName"
    $script:completedActivities = 0
    $script:percentComplete = 0
    $script:currentActivity = ""
    $script:totalActivities = `
       $moduleList.Count + 1

    function ReportStartOfActivity($activity) 
    {
       $script:currentActivity = $activity
       Write-Progress -Activity $loadingActivity -CurrentOperation $script:currentActivity -PercentComplete $script:percentComplete
    }
    
    function ReportFinishedActivity() 
    {
       $script:completedActivities++
       $script:percentComplete = (100.0 / $totalActivities) * $script:completedActivities
       $script:percentComplete = [Math]::Min(99, $percentComplete)
   
       Write-Progress -Activity $loadingActivity -CurrentOperation $script:currentActivity -PercentComplete $script:percentComplete
    }

    # Load modules
    function LoadModules()
    {
       ReportStartOfActivity "Searching for $productShortName module components..."
   
       $loaded = Get-Module -Name $moduleList -ErrorAction Ignore | % {$_.Name}
       $registered = Get-Module -Name $moduleList -ListAvailable -ErrorAction Ignore | % {$_.Name}
       $notLoaded = $registered | ? {$loaded -notcontains $_}
   
       ReportFinishedActivity
   
       foreach ($module in $registered) {
          if ($loaded -notcontains $module) {
		     ReportStartOfActivity "Loading module $module"
         
		     Import-Module $module
		 
		     ReportFinishedActivity
          }
       }
    }

    LoadModules

    # Update PowerCLI version after snap-in load
    $powerCliFriendlyVersion = [VMware.VimAutomation.Sdk.Util10.ProductInfo]::PowerCLIFriendlyVersion
    $host.ui.RawUI.WindowTitle = $powerCliFriendlyVersion

    # Launch text
    write-host "          Welcome to VMware $productName!"
    write-host ""
    write-host "Log in to a vCenter Server or ESX host:              " -NoNewLine
    write-host "Connect-VIServer" -foregroundcolor yellow
    write-host "To find out what commands are available, type:       " -NoNewLine
    write-host "Get-VICommand" -foregroundcolor yellow
    write-host "To show searchable help for all PowerCLI commands:   " -NoNewLine
    write-host "Get-PowerCLIHelp" -foregroundcolor yellow  
    write-host "Once you've connected, display all virtual machines: " -NoNewLine
    write-host "Get-VM" -foregroundcolor yellow
    write-host "If you need more help, visit the PowerCLI community: " -NoNewLine
    write-host "Get-PowerCLICommunity" -foregroundcolor yellow
    write-host ""
    write-host "       Copyright (C) VMware, Inc. All rights reserved."
    write-host ""
    write-host ""

    Write-Progress -Activity $loadingActivity -Completed

    cd \

}

$VMWDepCheckPath = Test-Path "C:\Program Files (x86)\VMware\Infrastructure\PowerCLI\Scripts\Initialize-PowerCLIEnvironment.ps1"
If($VMWDepCheckPath -eq $false)
{
    $DepBoxWarning = [System.Windows.MessageBox]::Show("The VMWare PowerCLI Module is not installed on this system, would you like to install it?",'Dependency Error','YesNo','Error')
    If($DepBoxWarning -eq 'Yes')
    {
        $IE = New-Object -ComObject InternetExplorer.Application
        $IE.Navigate2("https://my.vmware.com/group/vmware/details?downloadGroup=PCLI650R1&productId=614")
        $IE.Visible = $True
        [System.Windows.MessageBox]::Show("The script will now exit, please re-run once VMWare PowerCLI 6.5 R1 has been installed.",'Info','Ok','Info')
    }
    ElseIf($DepBoxWarning -eq 'No')
    {
        $Err = Read-Host -Prompt "The script will now exit as one or more dependencies could not be installed. `n[PRESS ANY KEY TO EXIT]"
        Exit
    }
}
ElseIf($VMWDepCheckPath -eq $true)
{
    $VMWDepCheck = Get-Module -ListAvailable
    If($VMWDepCheck.Name -notcontains "VMware.VimAutomation.Extensions")
    {
        $DepBoxWarning = [System.Windows.MessageBox]::Show("The VMWare PowerCLI Extensions Module is not installed on this system or has not installed properly, this is required to manipulate vFRC and vSAN, would you like to install it?",'Dependency Error','YesNo','Error')
        If($DepBoxWarning -eq 'Yes')
        {
            mkdir C:/Temp -ErrorAction SilentlyContinue
            Invoke-WebRequest -Uri "https://download3.vmware.com/software/vmw-tools/powercliext/VMware.VimAutomation.Extensions_for_PCLI_65R1.zip" -OutFile "C:/Temp/VMware.VimAutomation.Extensions_for_PCLI_65R1.zip"
            Expand-Archive "C:/Temp/VMware.VimAutomation.Extensions_for_PCLI_65R1.zip" -DestinationPath "C:/Temp/VMware.VimAutomation.Extensions_for_PCLI_65R1/" -Force
            mkdir 'C:\Program Files\WindowsPowerShell\Modules\VMware.VimAutomation.Extensions' -ErrorAction SilentlyContinue
            Get-ChildItem -Path "C:\Temp\VMware.VimAutomation.Extensions_for_PCLI_65R1\Modules\VMware.VimAutomation.Extensions" -Recurse | Move-Item -Destination "C:\Program Files\WindowsPowerShell\Modules\VMware.VimAutomation.Extensions"
            [System.Windows.MessageBox]::Show("The VMWare PowerCLI Extensions module has been installed, the script will now exit. Please re-run the script.",'Info','Ok','Info')
            Exit
        }
        If($DepBoxWarning -eq 'No')
        {
            $Err = Read-Host -Prompt "The script will now exit as one or more dependencies could not be met. `n[PRESS ANY KEY TO EXIT]"
            Exit
        }
    }
}

ImportPowerCLI

$VCCredential = Get-Credential -Message "Please enter your administrator credentials, please ensure they will grant you access to $VCenterServer via HTTPS."
Connect-VIServer -Server $VCenterServer -Credential $VCCredential

$VMSelection = Select-TargetVM

$VFRCParams = VRFCParameterBox

Disconnect-VIServer -Confirm